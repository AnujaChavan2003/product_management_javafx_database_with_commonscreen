package delete_product;

import java.sql.Connection;
import java.sql.Statement;

import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import show_option.ShowOption;

public class DeleteProductController {

	static Connection con;
	static Statement stmt;
	
	@FXML
	private TextField productName;
	@FXML
	private Button Delete;
	@FXML
	private Button next;
	@FXML
	private Button back;
	
	public void DeleteProduct(ActionEvent event) {
		System.out.println(productName.getText());
		String query =" delete from Product where Name ='" + productName.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		new ShowOption().Show(); 
	}
}
